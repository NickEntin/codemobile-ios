//
//  CDEditor.m
//  Code
//
//  Created by Nick Entin on 11/8/14.
//  Copyright (c) 2014 Nick Entin. All rights reserved.
//

#import "CDEditor.h"

static CDEditor *sharedInstance;

@implementation CDEditor

+ (CDEditor*)sharedInstance {
    if (!sharedInstance) {
        sharedInstance = [[CDEditor alloc] init];
    }
    return sharedInstance;
}

- (id)init {
    if (self = [super init]) {
        NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"CodeEditor" ofType:@"html"]];
        [self loadRequest:[NSURLRequest requestWithURL:url]];
        self.scrollView.scrollEnabled = false;
        
        self.plugins = [NSMutableArray array];
        
        // debug
        [self loadDefaultPlugins];
    }
    return self;
}

- (void)loadDefaultPlugins {
    CDPlugin *setThemeTwilight = [[CDPlugin alloc] init];
    setThemeTwilight.displayName = @"Theme - Twilight";
    setThemeTwilight.name = @"setTheme";
    setThemeTwilight.arguments = @[ @"\"twilight\"" ];
    
    CDPlugin *setThemeXcode = [[CDPlugin alloc] init];
    setThemeXcode.displayName = @"Theme - Xcode";
    setThemeXcode.name = @"setTheme";
    setThemeXcode.arguments = @[ @"\"xcode\"" ];
    
    CDPlugin *setThemeCobalt = [[CDPlugin alloc] init];
    setThemeCobalt.displayName = @"Theme - Cobalt";
    setThemeCobalt.name = @"setTheme";
    setThemeCobalt.arguments = @[ @"\"cobalt\"" ];
    
    CDPluginGroup *themes = [[CDPluginGroup alloc] init];
    themes.displayName = @"Themes";
    themes.members = @[ setThemeTwilight, setThemeXcode, setThemeCobalt ];
    [self.plugins addObject:themes];
    
    CDPlugin *goToLine = [[CDPlugin alloc] init];
    goToLine.displayName = @"Jump to Top";
    goToLine.name = @"goToLine";
    goToLine.arguments = @[ @"0" ];
    [self.plugins addObject:goToLine];
    
    CDPlugin *moveLineUp = [[CDPlugin alloc] init];
    moveLineUp.displayName = @"Move Line Up";
    moveLineUp.name = @"swapLines";
    moveLineUp.arguments = @[ @"true" ];
    [self.plugins addObject:moveLineUp];
    
    CDPlugin *moveLineDown = [[CDPlugin alloc] init];
    moveLineDown.displayName = @"Move Line Down";
    moveLineDown.name = @"swapLines";
    moveLineDown.arguments = @[ @"false" ];
    [self.plugins addObject:moveLineDown];
    
    // add general scroll plugin
    
    
    CDPlugin *scrollUp = [[CDPlugin alloc] init];
    scrollUp.displayName = @"Page Up";
    scrollUp.name = @"scroll";
    scrollUp.arguments = @[ @"true" ];
    [self.plugins addObject:scrollUp];
    
    CDPlugin *scrollDown = [[CDPlugin alloc] init];
    scrollDown.displayName = @"Page Down";
    scrollDown.name = @"scroll";
    scrollDown.arguments = @[ @"false" ];
    [self.plugins addObject:scrollDown];
}

- (void)loadFile:(CDFile*)file {
    NSLog(@"Loading file: %@", file.name);
    
    NSString *data = file.content;
    data = [data stringByReplacingOccurrencesOfString:@"\\" withString:@"\\\\"];
    data = [data stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
    data = [data stringByReplacingOccurrencesOfString:@"\n" withString:@"\\n"];
    
    [self triggerPlugin:@"loadFile" withArguments:@[[NSString stringWithFormat:@"\"%@\"",data]]];
}

- (NSString*)triggerPlugin:(NSString*)plugin withArguments:(NSArray*)arguments {
    // generate method string
    NSMutableString *method = [NSMutableString stringWithString:[NSString stringWithFormat:@"plugins.%@.execute({}",plugin]];
    for (NSString *arg in arguments) {
        [method appendFormat:@",%@",arg];
    }
    [method appendString:@");"];
    
    // run method in webview
    return [self stringByEvaluatingJavaScriptFromString:method];
}

@end
