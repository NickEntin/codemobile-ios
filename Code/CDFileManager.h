//
//  CDFileManager.h
//  Code
//
//  Created by Nick Entin on 11/8/14.
//  Copyright (c) 2014 Nick Entin. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CDDeliveryService.h"
#import "CDPackage.h"
#import "CDFile.h"

@protocol CDFileManagerDelegate <NSObject>

@optional

// package listeners
- (void)fileManagerDidAddPackage:(CDPackage*)package;

// file listeners
- (void)fileManagerDidUpdatePackage:(CDPackage*)package;

@end

@interface CDFileManager : NSObject

@property (nonatomic, readwrite) NSMutableArray *listeners;

// Shared Instance
+ (CDFileManager*)sharedInstance;

// add listeners
- (void)addPackageListener:(id<CDFileManagerDelegate>)listener;
- (void)addFileListener:(id<CDFileManagerDelegate>)listener toPackage:(CDPackage*)package;

@end
