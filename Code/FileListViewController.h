//
//  FileListViewController.h
//  Code
//
//  Created by Nick Entin on 11/8/14.
//  Copyright (c) 2014 Nick Entin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CDDeliveryService.h"

#import "DetailViewController.h"

@interface FileListViewController : UITableViewController

@property (nonatomic, readwrite) CDPackage *package;
@property (nonatomic, readwrite) DetailViewController *detailViewController;

@end
