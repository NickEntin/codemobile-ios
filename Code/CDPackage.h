//
//  CDPackage.h
//  Code
//
//  Created by Nick Entin on 11/8/14.
//  Copyright (c) 2014 Nick Entin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CDPackage : NSObject

@property (nonatomic, readwrite) NSString *packageId;       // UUID of package
@property (nonatomic, readwrite) NSDate *date;              // date/time when originally sent
@property (nonatomic, readwrite) NSString *sender;          // UUID of sender
@property (nonatomic, readwrite) NSMutableArray *files;     // mutable array of CDFile objects included in package

// listeners (for use in CDFileManager)
@property (nonatomic, readwrite) NSMutableArray *listeners;

// Return Functions
- (void)returnToSender;

@end
