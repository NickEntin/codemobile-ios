//
//  CDPluginGroup.h
//  Code
//
//  Created by Nick Entin on 11/9/14.
//  Copyright (c) 2014 Nick Entin. All rights reserved.
//

#import "CDPlugin.h"

@interface CDPluginGroup : CDPlugin

@property (nonatomic, readwrite) NSArray *members;

- (BOOL)isGroup;

@end
