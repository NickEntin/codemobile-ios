//
//  CDEditor.h
//  Code
//
//  Created by Nick Entin on 11/8/14.
//  Copyright (c) 2014 Nick Entin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CDFile.h"
#import "CDPlugin.h"
#import "CDPluginGroup.h"

@interface CDEditor : UIWebView

@property (nonatomic, readwrite) NSMutableArray *plugins;

+ (CDEditor*)sharedInstance;

- (void)loadFile:(CDFile*)file;
- (void)loadPlugin:(CDPlugin*)plugin;
- (NSString*)triggerPlugin:(NSString*)plugin withArguments:(NSArray*)arguments;

@end
