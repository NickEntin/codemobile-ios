//
//  DetailViewController.m
//  Code
//
//  Created by Nick Entin on 11/7/14.
//  Copyright (c) 2014 Nick Entin. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

#pragma mark - Managing the detail item

- (void)setDetailItem:(CDFile*)newDetailItem {
    if (_file != newDetailItem) {
        _file = newDetailItem;
            
        // Update the view.
        [self configureView];
    }
}

- (void)configureView {
    // Update the user interface for the detail item.
    if (self.file) {
        self.title = self.file.name;
        [self.editor loadFile:self.file];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self configureView];
    
    self.editor = [CDEditor sharedInstance];
    [self.view addSubview:self.editor];
    [self.editor setFrame:self.view.bounds];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
