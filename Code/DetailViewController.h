//
//  DetailViewController.h
//  Code
//
//  Created by Nick Entin on 11/7/14.
//  Copyright (c) 2014 Nick Entin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CDFile.h"
#import "CDEditor.h"

@interface DetailViewController : UIViewController

@property (strong, nonatomic) CDFile *file;
@property (weak, nonatomic) IBOutlet CDEditor *editor;

@end

