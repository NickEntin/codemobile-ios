//
//  CDFileManager.m
//  Code
//
//  Created by Nick Entin on 11/8/14.
//  Copyright (c) 2014 Nick Entin. All rights reserved.
//

#import "CDFileManager.h"

static CDFileManager *sharedInstance;

@implementation CDFileManager

#pragma mark - Shared Instance

+ (CDFileManager*)sharedInstance {
    if (!sharedInstance) {
        sharedInstance = [[CDFileManager alloc] init];
    }
    return sharedInstance;
}

#pragma mark - Initialization

- (id)init {
    if (self = [super init]) {
        self.listeners = [NSMutableArray array];
    }
    return self;
}

#pragma mark - Add Listeners

- (void)addPackageListener:(id<CDFileManagerDelegate>)listener {
    [self.listeners addObject:listener];
}

- (void)addFileListener:(id<CDFileManagerDelegate>)listener toPackage:(CDPackage *)package {
    [package.listeners addObject:listener];
}

#pragma mark - Trigger Listeners

- (void)deliveryServiceDidReceivePackage:(CDPackage*)package {
    for (int i = 0 ; i < [self.listeners count] ; i++) {
        if ([[self.listeners objectAtIndex:i] respondsToSelector:@selector(fileManagerDidAddPackage:)]) {
            [[self.listeners objectAtIndex:i] fileManagerDidAddPackage:package];
        }
    }
}

- (void)deliveryServiceDidUpdatePackage:(CDPackage*)package {
    for (int i = 0 ; i < [package.listeners count] ; i++) {
        if ([[package.listeners objectAtIndex:i] respondsToSelector:@selector(fileManagerDidUpdatePackage:)]) {
            [[package.listeners objectAtIndex:i] fileManagerDidUpdatePackage:package];
        }
    }
}

@end
