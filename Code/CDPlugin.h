//
//  CDPlugin.h
//  Code
//
//  Created by Nick Entin on 11/9/14.
//  Copyright (c) 2014 Nick Entin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CDPlugin : NSObject

@property (nonatomic, readwrite) NSString *displayName;
@property (nonatomic, readwrite) NSString *name;
@property (nonatomic, readwrite) NSArray *arguments;

// master plugins should have
@property (nonatomic, readwrite) BOOL isMaster;
@property (nonatomic, readwrite) NSString *script;

// for compatibilty with group plugins
- (BOOL)isGroup;

@end
