//
//  main.m
//  Code
//
//  Created by Nick Entin on 11/7/14.
//  Copyright (c) 2014 Nick Entin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
