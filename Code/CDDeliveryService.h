//
//  CDDeliveryService.h
//  Code
//
//  Created by Nick Entin on 11/8/14.
//  Copyright (c) 2014 Nick Entin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Dropbox/Dropbox.h>

#import "CDPackage.h"
#import "CDFile.h"

@interface CDDeliveryService : NSObject

// device properties
@property (nonatomic, readwrite) NSString *uuid;            // current device's UUID
@property (nonatomic, readwrite) NSString *deviceName;      // current device's name

// Dropbox data
@property (nonatomic, readwrite) BOOL hasLinkedAccount;     // whether there is a linked Dropbox account
@property (nonatomic, readwrite) DBDatastore *datastore;    // Dropbox datastore

// local data
@property (nonatomic, readwrite) NSMutableArray *packages;  // mutable array of CDPackage objects

// timers
@property (nonatomic, readwrite) NSTimer *pingTimer;

// Shared Instance
+ (CDDeliveryService*)sharedInstance;

// Initialization
- (CDDeliveryService*)init;
- (void)setupDatastore;
- (void)setupFilesystem;

// Interactions
- (NSMutableArray*)getPackages;
- (NSString*)nameForDeviceWithId:(NSString*)duuid;
- (void)returnPackageToSender:(CDPackage*)package;

@end
