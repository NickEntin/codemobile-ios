//
//  CDDeliveryService.m
//  Code
//
//  Created by Nick Entin on 11/8/14.
//  Copyright (c) 2014 Nick Entin. All rights reserved.
//

#import "CDDeliveryService.h"

static CDDeliveryService *sharedInstance;

// ping interval in seconds
static const int PING_INTERVAL = 10;

@implementation CDDeliveryService

#pragma mark - Shared Instance

+ (CDDeliveryService*)sharedInstance {
    if (!sharedInstance) {
        sharedInstance = [[CDDeliveryService alloc] init];
    }
    return sharedInstance;
}

#pragma mark - Initialization

- (CDDeliveryService*)init {
    if (self = [super init]) {
        // read Dropbox config data from DropboxConfig.plist
        NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:[[NSBundle mainBundle] pathForResource:@"DropboxConfig" ofType:@"plist"]];
        NSError *error = nil;
        NSPropertyListFormat format;
        NSDictionary *config = (NSDictionary *)
        [NSPropertyListSerialization propertyListWithData:plistXML
                                                  options:NSPropertyListMutableContainersAndLeaves
                                                   format:&format
                                                    error:&error];
        if (!config) {
            NSLog(@"Error reading Dropbox config data: %@ (format: %d)", [error description], (int)format);
        }
        
        // set up Dropbox account manager
        [DBAccountManager setSharedManager:[[DBAccountManager alloc] initWithAppKey:[config objectForKey:@"AppKey"] secret:[config objectForKey:@"AppSecret"]]];
        
        // set up Dropbox datastore manager
        DBAccount *account = [[DBAccountManager sharedManager] linkedAccount];
        if (account) {
            // use Dropbox datastores
            NSLog(@"Using remote datastore");
            [DBDatastoreManager setSharedManager:[DBDatastoreManager managerForAccount:account]];
            
            self.hasLinkedAccount = true;
            [self setupDatastore];
            [self setupFilesystem];
        } else {
            self.hasLinkedAccount = false;
        }
    }
    return self;
}

- (void)setupDatastore {
    // get existing UUID or generate new one if none exists
    if ([[NSUserDefaults standardUserDefaults] stringForKey:@"DeviceID"] == nil) {
        [[NSUserDefaults standardUserDefaults] setObject:[[NSUUID UUID] UUIDString] forKey:@"DeviceID"];
        [[NSUserDefaults standardUserDefaults] setObject:@"My Device" forKey:@"DeviceName"];
    }
    self.uuid = [[NSUserDefaults standardUserDefaults] stringForKey:@"DeviceID"];
    self.deviceName = [[NSUserDefaults standardUserDefaults] stringForKey:@"DeviceName"];
    
    // set up datastore
//    self.datastore = [[DBDatastoreManager sharedManager] openDefaultDatastore:nil];
    self.datastore = [[DBDatastoreManager sharedManager] openDatastore:@"code_mobile" error:nil];
    
    DBTable *deviceTable = [self.datastore getTable:@"devices"];
    NSArray *devices = [deviceTable query:@{ @"deviceID":self.uuid } error:nil];
    if ([devices count] == 0) {
        [deviceTable insert:@{
                              @"name":self.deviceName,
                              @"deviceID":self.uuid,
                              @"lastActive":[NSDate date]
                              }];
    }
    [self.datastore sync:nil];
    
    // set up ping timer
    self.pingTimer = [NSTimer scheduledTimerWithTimeInterval:PING_INTERVAL target:self selector:@selector(ping) userInfo:nil repeats:true];
    [self ping];
}

- (void)setupFilesystem {
    DBAccount *account = [[DBAccountManager sharedManager] linkedAccount];
    if (account) {
        DBFilesystem *filesystem = [[DBFilesystem alloc] initWithAccount:account];
        [DBFilesystem setSharedFilesystem:filesystem];
    }
}

#pragma mark - Public Getters/Setters

- (NSMutableArray*)getPackages {
    if (self.packages == nil) [self loadPackages];
    return self.packages;
}

- (NSString*)nameForDeviceWithId:(NSString*)duuid {
    DBTable *deviceTable = [self.datastore getTable:@"devices"];
    NSArray *devices = [deviceTable query:@{ @"deviceID":duuid } error:nil];
    return [devices count] == 0 ? @"[INVALID UUID]" : [[devices objectAtIndex:0] objectForKey:@"name"];
}

- (void)returnPackageToSender:(CDPackage*)package {
    
}

#pragma mark - Data Manipulations

- (void)loadPackages {
    DBTable *packageTable = [self.datastore getTable:@"packages"];
    NSArray *qresult = [packageTable query:@{ @"toID":self.uuid } error:nil];
    
    self.packages = [NSMutableArray arrayWithCapacity:[qresult count]];
    for (DBRecord *record in qresult) {
        CDPackage *package = [[CDPackage alloc] init];
        
        package.packageId = [record objectForKey:@"packageID"];
        package.date = [record objectForKey:@"date"];
        package.sender = [record objectForKey:@"fromID"];
        
        package.files = [NSMutableArray arrayWithCapacity:[[record getOrCreateList:@"files"].values count]];
        for (NSString *fname in [record getOrCreateList:@"files"].values) {
            DBFile *dfile = [[DBFilesystem sharedFilesystem] openFile:[[DBPath root] childPath:[NSString stringWithFormat:@"Apps/CodeMobile/%@/%@",package.packageId,fname]] error:nil];
            
            CDFile *file = [[CDFile alloc] init];
            file.name = fname;
            file.content = [dfile readString:nil];
            
            [package.files addObject:file];
        }
        
        [self.packages addObject:package];
    }
}

- (void)ping {
    DBTable *deviceTable = [self.datastore getTable:@"devices"];
    NSArray *devices = [deviceTable query:@{ @"deviceID":self.uuid } error:nil];
    [((DBRecord*)[devices objectAtIndex:0]) update:@{ @"lastActive":[NSDate date] }];
    [self.datastore sync:nil];
}

@end
