//
//  CDFile.h
//  Code
//
//  Created by Nick Entin on 11/8/14.
//  Copyright (c) 2014 Nick Entin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CDFile : NSObject

@property (nonatomic, readwrite) NSString *name;            // filename
@property (nonatomic, readwrite) NSString *content;         // text contents

@end
