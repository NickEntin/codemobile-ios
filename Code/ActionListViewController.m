//
//  ActionListViewController.m
//  Code
//
//  Created by Nick Entin on 11/9/14.
//  Copyright (c) 2014 Nick Entin. All rights reserved.
//

#import "ActionListViewController.h"

@interface ActionListViewController ()

@property (nonatomic, readwrite) NSArray *plugins;

@end

@implementation ActionListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.plugins == nil) {
        self.plugins = [[CDEditor sharedInstance] plugins];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.plugins count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CDPlugin *plugin = [self.plugins objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:(plugin.isGroup ? @"GroupPluginCell" : @"PluginCell") forIndexPath:indexPath];
    
    cell.textLabel.text = plugin.displayName;
    
    
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    CDPluginGroup *plugin = [self.plugins objectAtIndex:[self.tableView indexPathForSelectedRow].row];
    ((ActionListViewController*)[segue destinationViewController]).plugins = plugin.members;
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    CDPlugin *plugin = [self.plugins objectAtIndex:[self.tableView indexPathForSelectedRow].row];
    
    if (plugin.isGroup) {
        return YES;
    } else {
        [[CDEditor sharedInstance] triggerPlugin:plugin.name withArguments:plugin.arguments];
        return NO;
    }
}

@end
