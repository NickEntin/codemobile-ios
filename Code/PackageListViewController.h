//
//  MasterViewController.h
//  Code
//
//  Created by Nick Entin on 11/7/14.
//  Copyright (c) 2014 Nick Entin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CDDeliveryService.h"

#import "FileListViewController.h"

@class DetailViewController;

@interface PackageListViewController : UITableViewController <UIAlertViewDelegate>

@property (strong, nonatomic) DetailViewController *detailViewController;


@end

