Code-iOS
========

CodeMobile app for iOS - a project started by Nick Entin and Alex Allsup at HackSC 2014.  Please note that this is a work in progress and has not been published to the app store.

The app integrates with Dropbox for its core functionality.  The API keys are not stored in the repo (yes - they were there originally during the hackathon but we have since changed them).  In order to get the app running, you need to add the config file:

/Code/DropboxConfig.plist
```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>AppKey</key>
	<string>(app key)</string>
	<key>AppSecret</key>
	<string>(app secret)</string>
</dict>
</plist>
```
